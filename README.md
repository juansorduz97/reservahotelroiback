# reservahotelroiback

The following repository is used to continue the roiback selection process

OS: Ubuntu 16.04

Python: 3.7.6

Django: 3.0.2

References:
  https://www.youtube.com/watch?v=UmljXZIypDc&list=PL-osiE80TeTtoQCKZ03TU5fNfx2UY6U4p

## Folders:

  initialdesign: Folder with initial design models

  reservahotel: Folder with djangoproject

## Administrator data:

  Usuario=ROIBACKAdmin1

  contraseña=roiback123

## Common traveler data:

  Usuario=Juan1

  contraseña=roiback123

## Running localhost django server:

  cd reservahotel

  python manage.py runserver
