# Generated by Django 3.0.2 on 2020-01-06 22:52

from django.db import migrations, models
import django.utils.timezone
import hotel.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Hotel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('city', models.CharField(choices=[(hotel.models.Cities['Bog'], 'Bogota'), (hotel.models.Cities['Med'], 'Medellin'), (hotel.models.Cities['Cal'], 'Cali'), (hotel.models.Cities['Car'], 'Cartagena')], max_length=8)),
                ('ubication', models.CharField(default='Ubication Hotel', max_length=100)),
                ('score', models.IntegerField(default=0)),
                ('description', models.TextField()),
                ('state', models.CharField(choices=[(hotel.models.States['Av'], 'Available'), (hotel.models.States['Unav'], 'Unavailable')], max_length=8)),
                ('date_posted', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
