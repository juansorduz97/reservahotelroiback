from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from enum import Enum
from django.core.validators import MinValueValidator, MaxValueValidator
from room.models import Room

class Cities(Enum):
    Bog = "Bogota"
    Med = "Medellin"
    Cal = "Cali"
    Car = "Cartagena"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

class States(Enum):
    Av = "Available"
    Unav = "Unavailable"

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)

class Hotel(models.Model):
    name = models.CharField(max_length=100)
    city = models.CharField(max_length=8, choices=Cities.choices())
    ubication = models.CharField(max_length=100, default="Ubication Hotel")
    score = models.IntegerField(default=0)
    stars = models.IntegerField(default=0, validators=[MinValueValidator(1), MaxValueValidator(5)])
    description = models.TextField()
    state = models.CharField(max_length=8, choices=States.choices())
    date_posted = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.name} Hotel'

    def get_absolute_url(self):
        return reverse('hotel-detail', kwargs={'pk': self.pk})
