from django.urls import path
from .views import(
    HotelListView,
    HotelDetailView,
    HotelCreateView,
    HotelUpdateView,
    HotelDeleteView,
)
from . import views as hotel_views

urlpatterns = [
    path('', HotelListView.as_view(), name='hotel-home'),
    #path('<int:pk>/', HotelDetailView.as_view(), name='hotel-detail'),
    path('<int:pk>/', hotel_views.hoteldetails, name='hotel-detail'),
    path('new/', HotelCreateView.as_view(), name='hotel-create'),
    path('<int:pk>/update/', HotelUpdateView.as_view(), name='hotel-update'),
    path('<int:pk>/delete/', HotelDeleteView.as_view(), name='hotel-delete'),
]
