from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.views.generic import(
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import Hotel
#from room.models import Room
from hotelroom.models import HotelRoom

def home(request):
    return render(request, 'hotel/home.html')

class HotelListView(ListView):
    model = Hotel
    template_name = 'hotel/home.html'
    context_object_name = 'hotels'
    ordering = ['-date_posted']
    paginate_by = 4


class HotelDetailView(DetailView):
    model = Hotel


class HotelCreateView(LoginRequiredMixin, CreateView):
    model = Hotel
    fields = ['name', 'city', 'ubication', 'score', 'stars', 'description', 'state']
    success_url = '/hotel/'

class HotelUpdateView(LoginRequiredMixin, UpdateView):
    model = Hotel
    fields = ['name', 'city', 'ubication', 'score', 'stars', 'description', 'state']
    success_url = '/hotel/'

class HotelDeleteView(LoginRequiredMixin, DeleteView):
    model = Hotel
    success_url = '/hotel/'

def hoteldetails(request, pk):
    hotel = Hotel.objects.get(id=pk)
    #print(hotel)
    #rooms_lists = Room.objects.order_by('-pub_date')
    hotels_rooms_lists = HotelRoom.objects.filter(hotel=hotel)
    context = {'hotel': hotel, 'hotelsrooms': hotels_rooms_lists}
    #print(context)
    #context = {'hotel': hotel, 'rooms': rooms_lists, 'hotels_rooms': hotels_rooms_lists}

    return render(request, 'hotel/hotel_detail2.html', context)
