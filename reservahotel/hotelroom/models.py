from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from enum import Enum
from django.core.validators import MinValueValidator, MaxValueValidator
from room.models import Room
from hotel.models import Hotel, States

class HotelRoom(models.Model):
    hotel = models.ForeignKey(Hotel, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    pricenight = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    state = models.CharField(default='Av', max_length=8, choices=States.choices())
    ubication = models.CharField(default='First Floor', max_length=100)

    def __str__(self):
        return f'{self.hotel.name} Hotel add {self.room.name} Room'
