from django.urls import path
from .views import(
    HotelRoomDetailView,
    HotelRoomCreateView,
    HotelRoomUpdateView,
    HotelRoomDeleteView,
)
from . import views as hotelroom_views

urlpatterns = [
    path('<int:pk>/', HotelRoomDetailView.as_view(), name='hotelroom-detail'),
    #path('<int:pk>/', hotelroom_views.hotelroomdetails, name='hotelroom-detail'),
    path('new/', HotelRoomCreateView.as_view(), name='hotelroom-create'),
    path('<int:pk>/update/', HotelRoomUpdateView.as_view(), name='hotelroom-update'),
    path('<int:pk>/delete/', HotelRoomDeleteView.as_view(), name='hotelroom-delete'),
]
