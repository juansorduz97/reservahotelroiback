from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.views.generic import(
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import HotelRoom
from room.models import Room
from hotel.models import Hotel


class HotelRoomDetailView(DetailView):
    model = HotelRoom


class HotelRoomCreateView(LoginRequiredMixin, CreateView):
    model = HotelRoom
    fields = ['hotel', 'room', 'pricenight', 'state', 'ubication']
    success_url = '/hotelroom/new/'

class HotelRoomUpdateView(LoginRequiredMixin, UpdateView):
    model = HotelRoom
    fields = ['hotel', 'room', 'pricenight', 'state', 'ubication']
    success_url = '/hotel/'

class HotelRoomDeleteView(LoginRequiredMixin, DeleteView):
    model = HotelRoom
    success_url = '/hotel/'
