from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from hotelroom.models import HotelRoom

class Reservation(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    hotelroom = models.ForeignKey(HotelRoom, on_delete=models.CASCADE)
    checkin = models.DateTimeField(default=timezone.now)
    checkout = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.user.name} user reservate {self.hotelroom.room.name} Room in {self.hotelroom.hotel.name} Hotel'
