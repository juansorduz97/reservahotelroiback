from django.urls import path
from .views import(
    ReservationListView,
    ReservationDetailView,
    ReservationCreateView,
    ReservationUpdateView,
    ReservationDeleteView,
)
from . import views as reservation_views

urlpatterns = [
    path('', reservation_views.home, name='reservation-home'),
    path('<int:pk>/', ReservationDetailView.as_view(), name='reservation-detail'),
    path('new/', ReservationCreateView.as_view(), name='reservation-create'),
    path('<int:pk>/update/', ReservationUpdateView.as_view(), name='reservation-update'),
    path('<int:pk>/delete/', ReservationDeleteView.as_view(), name='reservation-delete'),
]
