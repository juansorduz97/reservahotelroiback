from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.views.generic import(
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import Reservation

class ReservationListView(ListView):
    model = Reservation
    template_name = 'reservation/home.html'
    context_object_name = 'reservations'
    ordering = ['-date_posted']
    paginate_by = 5

class ReservationDetailView(DetailView):
    model = Reservation


class ReservationCreateView(LoginRequiredMixin, CreateView):
    model = Reservation
    fields = ['user', 'hotelroom', 'checkin', 'checkout']
    success_url = '/hotel/'

    def get_form(self, *args, **kwargs):
        form = super(ReservationCreateView, self).get_form(*args, **kwargs)
        form.fields['user'].queryset = User.objects.filter(username=self.request.user.username)
        #form.instance.user=self.request.user
        return form
"""
    def form_valid(self, form):
        user = get_object_or_404(User, slug=self.kwargs['user'])
        form.instance.user = user
        return super(ReservationCreateView, self).form_valid(form)

"""

class ReservationUpdateView(LoginRequiredMixin, UpdateView):
    model = Reservation
    fields = ['hotelroom', 'checkin', 'checkout']
    success_url = '/hotel/'

"""
    def get_form(self, *args, **kwargs):
        form = super(ReservationUpdateView, self).get_form(*args, **kwargs)
        form.fields['user'].queryset = User.objects.filter(username=self.request.user.username)
        #form.instance.user=self.request.user
        return form
"""

class ReservationDeleteView(LoginRequiredMixin, DeleteView):
    model = Reservation
    success_url = '/hotel/'

def home(request):
    context = {
        'reservations': Reservation.objects.all()
    }
    return render(request, 'reservation/home.html', context)
