from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

class Room(models.Model):
    name = models.CharField(max_length=100)
    capacity = models.IntegerField(default=0)
    description = models.TextField()
    date_posted = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.name} Room'

    def get_absolute_url(self):
        return reverse('room-detail', kwargs={'pk': self.pk})
