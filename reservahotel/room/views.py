from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.views.generic import(
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView
)
from .models import Room

def home(request):
    return render(request, 'room/home.html')

class RoomListView(ListView):
    model = Room
    template_name = 'room/home.html'
    context_object_name = 'rooms'
    ordering = ['-date_posted']
    paginate_by = 4


class RoomDetailView(DetailView):
    model = Room

class RoomCreateView(LoginRequiredMixin, CreateView):
    model = Room
    fields = ['name', 'capacity', 'description']
    success_url = '/room/'

class RoomUpdateView(LoginRequiredMixin, UpdateView):
    model = Room
    fields = ['name', 'capacity', 'description']
    success_url = '/room/'

class RoomDeleteView(LoginRequiredMixin, DeleteView):
    model = Room
    success_url = '/room/'
